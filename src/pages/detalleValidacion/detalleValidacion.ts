import { Component, OnInit, OnChanges } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { LoginPage } from '../login/login';
import { Carrusel } from '../carrusel/carrusel';

@Component({
  selector: 'detalle-validacion',
  templateUrl: 'detalleValidacion.html'
})
export class DetalleValidacion implements OnInit, OnChanges{

  public validacion;
  public boletos;
  public juego;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authService: AuthserviceProvider,
    ) {
      this.validacion = navParams.get('validacion');
      this.boletos = navParams.get('boletos');
      this.juego = navParams.get('juego');

  }

  ionViewCanEnter() {
    if(this.authService.isAuthenticated()){
      return true;
    }else{
      this.navCtrl.setRoot(LoginPage);
    }
  }

  ngOnInit (): void {
    this.iniciarBoletos();
  }

  ngOnChanges() {
    this.iniciarBoletos();
  }

  iniciarBoletos() {
    this.boletos = this.boletos.filter(e => e.validacion == this.validacion.num_validacion);
    if (navigator && navigator['notification'] && navigator['notification'].beep) {
      navigator['notification'].beep(1);
    }
  }

  verCarrusel(boleto) {
    if (boleto.clon != undefined && boleto.clon != 0) {
      let b = this.boletos.find(e => e.id_boleto == boleto.clon);
      boleto = b;
    }
    this.navCtrl.push(Carrusel, {
      validacion: this.validacion,
      boletos: this.boletos,
      boleto: boleto,
      juego: this.juego
    });
  }
  
}
