import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { HomePage } from '../home/home';
import { IpProvider } from '../../providers/ipservice/ipservice';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authService: AuthserviceProvider,
    private ipservice: IpProvider,
    private toastCtrl: ToastController) {
  }

  @ViewChild('email') email: any;
  private username: string = '';
  private password: string = '';
  public serverUrl = '';

  ngOnInit(): void {
    this.serverUrl = this.ipservice.getBaseUrl();
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.email.setFocus();
    }, 500);
  }

  cambiarUrlServidor () {
    this.ipservice.setBaseUrl(this.serverUrl);
  }

  login(){
    
    return this.authService.login(this.username, this.password)
      .subscribe(
        data => {
          this.navCtrl.setRoot(HomePage);
        },
        error => {
          this.presentErrorToast(error._body);
        }
      );

  }

  presentErrorToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: 'toast-error'
    });
  
    toast.onDidDismiss(() => {});
  
    toast.present();
  }

}
