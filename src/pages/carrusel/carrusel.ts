import { Component, OnInit, OnChanges, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { LoginPage } from '../login/login';
import QRCode from 'qrcode';

@Component({
  selector: 'carrusel',
  templateUrl: 'carrusel.html'
})
export class Carrusel implements OnInit, OnChanges {

  @ViewChild(Slides) slides: Slides;

  public validacion;
  public boletos;
  public boleto;
  public juego;
  public code = '';
  public generated;
  public tipo_sorteo;
  public id_receptor;
  public codi_lae;
  public fechaSorteo;
  public NUMERO_A;
  public NUMERO_P;
  public apuestas = [];
  public codigoQrBoleto = '';
  public DIAS_TEXTO = {
    '0': 'Domingo', '1': 'Lunes', '2': 'Martes', '3': 'Miércoles', '4': 'Jueves', '5': 'Viernes', '6': 'Sábado', '7': 'Domingo',
    '123456': 'Semanal', '46': 'Semanal', '25': 'Semanal'
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authService: AuthserviceProvider,
    ) {
      
  }

  ionViewCanEnter() {
    if(this.authService.isAuthenticated()){
      return true;
    }else{
      this.navCtrl.setRoot(LoginPage);
    }
  }

  ngOnInit (): void {
    this.validacion = this.navParams.get('validacion');
    this.boletos = this.navParams.get('boletos');
    this.boleto = this.navParams.get('boleto');
    this.juego = this.navParams.get('juego');
    this.tipo_sorteo = this.validacion.tipo_sorteo;
    this.codi_lae = this.validacion.codi_lae;
    this.fechaSorteo = this.validacion.fechaSorteo;
    this.NUMERO_A = this.validacion.numero_a;
    this.NUMERO_P = this.validacion.numero_p;
    this.id_receptor = localStorage.getItem('idReceptor');
    this.iniciarBoletos();
  }

  ngOnChanges() {
    // this.iniciarBoletos();
  }

  iniciarBoletos() {
    if (this.tipo_sorteo == 1) {
      this.boletos = this.boletos.filter(e => e.validacion == this.validacion.num_validacion);
    } else {
      this.boletos = this.boletos.filter(e => e.validacion == this.validacion.num_validacion && e.clon == 0);
    }
    
    let posicion = this.boletos.findIndex(e => this.boleto.id_boleto == e.id_boleto);
    if (posicion == 0) {
      if (this.boleto.apuestas) {
        this.apuestas = this.boleto.apuestas.split(':');
      } else {
        this.apuestas = [this.boleto.apuesta];
      }
      this.generarQr(this.boleto);
    } else {
      setTimeout(() => {
        this.slides.slideTo(posicion);
      }, 1000);
    }
  }

  slideChanged() {
    this.generated = null;
    this.codigoQrBoleto = '';
    let currentIndex = this.slides.getActiveIndex();
    this.boleto = this.boletos[currentIndex];
    if (this.boleto) {
      if (this.boleto.apuestas) {
        this.apuestas = this.boleto.apuestas.split(':');
      } else {
        this.apuestas = [this.boleto.apuesta];
      }
      this.generarQr(this.boleto);
    }
  }

  addZeros(s) {
    if (s.toString().length == 1) {
      return `0${s}`;
    } else {
      return s;
    }
  }

  generarQr(boleto) {
    const qrcode = QRCode;
    const self = this;
    let parametroW = 0;
    let multiplicador = 1;
    if (this.tipo_sorteo == 2) {
      parametroW = 1;
      multiplicador = 6;
    }

    if (this.juego == 'Quinigol' || this.juego == 'Elige8') {
      parametroW = 0;
      multiplicador = 1;
    }

    
    let codigo = `A=${this.NUMERO_A};P=${this.NUMERO_P};S=${this.codi_lae}${this.fechaSorteo}:${multiplicador};W=${parametroW};`;
    if (this.juego == 'Bonoloto') {
      boleto.apuestas.split(':').reduce((s, c, idx) => {
        return codigo += `.${idx+1}=${c.replace(/,/g,'')}`;
      }, codigo);
      codigo += `;T=${this.id_receptor}`;
    } else if (this.juego == 'Gordo') {
      boleto.apuestas.split(':').reduce((s, c, idx) => {
        let numeros = c.split('+')[0];
        let clave = c.split('+')[1];
        if (idx == 0) {
          return codigo += `.${idx+1}=${numeros.replace(/,/g,'')}:${this.addZeros(clave)}`;
        } else {
          return codigo += `.${idx+1}=${numeros.replace(/,/g,'')}`;
        }
      }, codigo);
      codigo += `;T=${this.id_receptor}`;
    } else if (this.juego == 'Euromillones') {
      boleto.apuestas.split(':').reduce((s, c, idx) => {
        return codigo += `.${idx+1}=${c.split('+')[0].replace(/,/g,'')}:${c.split('+')[1].replace(/,/g,'')}`;
      }, codigo);
      codigo += `;T=${this.id_receptor};RI=11`;
    } else if (this.juego == 'Quinigol') {
      boleto.apuestas.split(':').reduce((s, c, idx) => {
        let apu = c.replace(/,/g,'');
        let apuArr = [];
        [0,2,4,6,8,10].forEach(i => {
          apuArr.push(apu.substring(i, i+2));
        });
        let cadena = '';
        [1,2,3,4,5,6].forEach((i, index) => {
          cadena += `${apuArr[index].substring(0,1)}-${apuArr[index].substring(1,2)}`;
        });
        return codigo += `.${idx+1}=${cadena}`;
      }, codigo);
      codigo += `;T=${this.id_receptor}`;
    } else if (this.juego == 'Elige8') {
      codigo += `.1=${boleto.apuesta};EF=13[B=1,FI=${boleto.elige8}];T=${this.id_receptor}`;
    }
    this.codigoQrBoleto = codigo;
    qrcode.toDataURL(codigo, { errorCorrectionLevel: 'H' }, (err, qr) => {
      self.generated = qr;
    })
  }
  
}
