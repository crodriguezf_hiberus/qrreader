import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { ToastController } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { LoginPage } from '../login/login';
import { Vibration } from '@ionic-native/vibration';
import { DetalleValidacion } from '../detalleValidacion/detalleValidacion';
import { Subject } from 'rxjs/Subject';
import { filter } from 'rxjs/operators';
import { IpProvider } from '../../providers/ipservice/ipservice';
import { Elige8Provider } from '../../providers/elige8/elige8';

@Component({
  selector: 'page-elige8',
  templateUrl: 'elige8.html'
})
export class Elige8Page implements OnInit, OnDestroy{

  public classHtml = '';
  public stringScanned = '';
  public scanSub;
  public validacion;
  public temporada;
  public num_sorteo;
  public validaciones;
  public verMarco = false;
  public escaneando = false;
  public dia;
  public fecha_sorteo;
  public boletos;
  public verMOverlay = false;
  public disableScanner = false;
  public modoDisparo;
  public modoError;
  public codigoPrevio;
  destroy$ = new Subject();
  request$;
  public baseUrl = 'https://api.eduardolosilla.es';
  

  constructor(
    public navCtrl: NavController, 
    public qrScanner: QRScanner, 
    private toastCtrl: ToastController,
    public authService: AuthserviceProvider,
    private ipservice: IpProvider,
    private quinigol: Elige8Provider,
    private vibration: Vibration) {
  }

  ionViewCanEnter() {
    if(this.authService.isAuthenticated()){
      return true;
    }else{
      this.navCtrl.setRoot(LoginPage);
    }
  }

  ngOnInit (): void {
    this.modoDisparo = localStorage.getItem('modoDisparo');
    this.modoError = parseInt(localStorage.getItem('modoError'), 10) || 0;
    this.obtenerInfoSorteo({complete: () => null});
    this.baseUrl = this.ipservice.getBaseUrl();
    this.codigoPrevio = '';
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  verDetalle (event) {
    this.obtenerInfoSorteo({complete: () => null}, true)
  }

  obtenerInfoSorteo(refresher, goto = false){
    this.codigoPrevio = '';
    let anterior = (this.validacion) ? this.validacion.num_validacion : 0;
    this.quinigol.obtenerInfoSorteo()
        .takeUntil(this.destroy$)
        .pipe(filter(data => data != null))
        .subscribe(data => {
          let body = JSON.parse(data['_body']);
          this.boletos = body.boletos;
          this.temporada = body.temporada;
          this.num_sorteo = body.jornada;
          this.validaciones = body.validaciones.map(e => {
            if (!e.pdf) {
              e.pdf = `control/convertirAsm?juego=elige8&destino=QR&jornada=${body.jornada}&temporada=${body.temporada}&prevalidacion=${e.num_validacion}`;
            }
            return e;
          });
          this.dia = body.dia;
          this.fecha_sorteo = body.fecha_sorteo;

          
          this.boletos.forEach(e => {
            let numDobles = 0;
            let numTriples = 0;
            let numDoblesElige8 = 0;
            let numTriplesElige8 = 0;
            let plenoAlQuince = 1;
            e.arrApuestasBoleto = e.apuesta;
            e.arrCodigosMillon = e.numero_a;
            const ap = e.apuesta.split(':')[0].split(',');
            ap.forEach(o => {
              if (o.length == 2) {
                numDobles++;
              } else if (o.length == 3) {
                numTriples++;
              }
            });
            plenoAlQuince = e.apuesta.split(':')[1].split(',').length;
            const apElige8 = [];
            for (let i = 0; i <= 15; i+=2) {
              apElige8.push(parseInt(e.elige8.substring(i, i+2)));
            }
            apElige8.forEach(o => {
              let signo = ap[o - 1];
              if (signo.length == 2) {
                numDoblesElige8++;
              } else if (o.length == 3) {
                numTriplesElige8++;
              }
            });
            e.totalApuestas = Math.pow(2, numDobles) * Math.pow(3, numTriples) * plenoAlQuince;
            e.totalApuestasElige8 = Math.pow(2, numDoblesElige8) * Math.pow(3, numTriplesElige8) * 1;
          });

          
          if(this.validacion == undefined || this.validacion == null || !this.isInArray(anterior, this.validaciones)){
            this.validacion = this.validaciones[0];
          }else {
            let pos = this.getPositionInArray(anterior, this.validaciones);
            this.validacion = this.validaciones[pos];
          }
          
          if (this.validacion) {
            this.validacion.totalApuestas = this.boletos.reduce((sum, curr) => sum += curr.totalApuestas, 0);
            this.validacion.totalApuestasElige8 = this.boletos.reduce((sum, curr) => sum += curr.totalApuestasElige8, 0);
          }

          refresher.complete();

          if (goto) {
            this.navCtrl.push(DetalleValidacion, {
              validacion: this.validacion,
              boletos: this.boletos,
              juego: 'Elige8'
            });
          }

        }, error => {
          refresher.complete();
          this.presentToastError(error);
        });
    
  }
  
  scan(){
    this.disableScanner = true;
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
            this.verMarco = true;
            this.escaneando = true;
            this.subscribeScanner();
          } else if (status.denied) {
            this.presentToast('No tiene permiso para usar la cámara del dispositivo');
          } else {
            this.presentToast('No tiene permiso para usar la cámara del dispositivo');
          }
      })
      .catch((e: any) => {
        this.initValues();
      });
  }

  subscribeScanner(){
    let html = document.getElementsByTagName('html');
    html[0].setAttribute('class', 'scanning');
    this.scanSub = this.qrScanner.scan()
      .subscribe((text: string) => {
        this.qrScanner.hide(); // hide camera preview
        this.vibration.vibrate(200);
        this.stringScanned = text;
        this.verMarco = false;
        this.scanSub.unsubscribe(); // stop scanning
        this.scanSub = null;
        if (this.stringScanned != this.codigoPrevio) {
          this.codigoPrevio = this.stringScanned;
          this.enviar();
          this.escaneando = false;
          this.initValues();
          if (this.modoDisparo != 0) {
            this.clickInMOverlay();
          }
        }
      });
    this.verMarco = true;
    this.qrScanner.show();
  }

  cancelarScanner(){
    this.disableScanner = false;
    this.verMarco = false;
    this.escaneando = false;
    this.initValues();
    let html = document.getElementsByTagName('html');
    html[0].classList.remove('scanning');
  }

  initValues(){
    if (this.qrScanner && this.modoDisparo == 0) {
      this.stringScanned = '';
      let html = document.getElementsByTagName('html');
      html[0].classList.remove('scanning');
      this.qrScanner.hide();
      this.verMarco = false;
    }
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'top',
      cssClass: 'toast-scanned'
    });
  
    toast.onDidDismiss(() => {
      this.verMOverlay = true;
    });
    toast.present();
  }

  emitirSonido() {
    if( window['plugins'] && window['plugins'].NativeAudio ) {
      window['plugins'].NativeAudio.play( 'click' );
    }
  }

  clickInMOverlay() {
    this.verMOverlay = false;
    this.verMarco = true;
    this.scan();
  }
  
  presentToastError(msg: string) {
    let toast;
    if (this.modoError === 0) {
      toast = this.toastCtrl.create({
        message: msg,
        position: 'top',
        cssClass: 'toast-error',
        showCloseButton: true
      });
    } else {
      toast = this.toastCtrl.create({
        message: msg,
        duration: 1000,
        position: 'top',
        cssClass: 'toast-error',
        showCloseButton: false
      });
    }
  
    toast.onDidDismiss(() => {
      this.verMOverlay = true;
    });
    toast.present();
  }

  public ultimo;

  enviar(){
    
    let anterior = this.validacion.num_validacion;
    if(this.validacion){
      return this.quinigol.enviarNumeroA({qr: this.stringScanned, validacion: this.validacion.num_validacion})
      .subscribe(data => {
        this.emitirSonido();
        this.initValues();
        this.presentToast('Código enviado con éxito');
        this.escaneando = false;
        let body = JSON.parse(data['_body']);
        this.temporada = body.temporada;
        this.num_sorteo = body.num_sorteo;
        this.validaciones = body.validaciones;

        if(this.validacion == undefined || this.validacion == null || !this.isInArray(anterior, this.validaciones)){
          this.validacion = this.validaciones[0];
        }else {
          let pos = this.getPositionInArray(anterior, this.validaciones);
          this.validacion = this.validaciones[pos];
        }
        
      }, error => {
        this.codigoPrevio = '';
        this.initValues();
        this.presentToastError(error._body);
        setTimeout(() => {
        }, 2000);
      });
    }else{
      this.presentToastError('Introduzca un número de validación');
    }
  }

  compareFn(e1: any, e2: any): boolean {
    return e1 && e2 ? e1.num_validacion === e2.num_validacion : e1 === e2;
  }

  isInArray(aguja, pajar){
    let response = false;
    for(let i = 0; i < pajar.length; i++){
      if(aguja == pajar[i].num_validacion){
        response = true;
        break;
      }

    }
    return response;
  }
  
  getPositionInArray(aguja, pajar){
    let response = -1;
    for(let i = 0; i < pajar.length; i++){
      if(aguja == pajar[i].num_validacion){
        response = i;
        break;
      }
    }
    return response;
  }
  

}


