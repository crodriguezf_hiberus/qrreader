import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AuthserviceProvider } from '../../providers/authservice/authservice';
import { LoginPage } from '../login/login';
import { Subject } from 'rxjs/Subject';
import { HomePage } from '../home/home';
import { BonolotoPage } from '../bonoloto/bonoloto';
import { QuinigolPage } from '../quinigol/quinigol';
import { GordoPage } from '../gordo/gordo';
import { LototurfPage } from '../lototurf/lototurf';
import { Elige8Page } from '../elige8/elige8';


@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html'
})
export class ConfiguracionPage implements OnInit, OnDestroy{

  public modoDisparo;
  public modoError;
  public idReceptor;
  destroy$ = new Subject();
  
  constructor(
    public navCtrl: NavController, 
    private toastCtrl: ToastController,
    public authService: AuthserviceProvider) {
  }

  ionViewCanEnter() {
    if(this.authService.isAuthenticated()){
      return true;
    }else{
      this.navCtrl.setRoot(LoginPage);
    }
  }

  ngOnInit (): void {
    let modoDisparo = localStorage.getItem('modoDisparo');
    if (modoDisparo != null) {
      this.modoDisparo = modoDisparo;
    } else {
      this.modoDisparo = 0;
    }
    let modoError = localStorage.getItem('modoError');
    if (modoError != null) {
      this.modoError = modoError;
    } else {
      this.modoError = 0;
    }
    let idReceptor = localStorage.getItem('idReceptor');
    if (idReceptor != null) {
      this.idReceptor = idReceptor;
    } else {
      this.idReceptor = '10825-2';
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'top',
      cssClass: 'toast-scanned'
    });
  
    toast.onDidDismiss(() => {
    });
    toast.present();
  }

  presentToastError(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      position: 'top',
      cssClass: 'toast-error',
      showCloseButton: true
    });
  
    toast.onDidDismiss(() => {
    });
    toast.present();
  }

  irA(pagina) {
    switch (pagina) {
      case 'euromillones':
        this.navCtrl.push(HomePage, {});
        break;
      case 'bonoloto':
        this.navCtrl.push(BonolotoPage, {});
        break; 
      case 'quinigol':
        this.navCtrl.push(QuinigolPage, {});
        break;
      case 'gordo':
        this.navCtrl.push(GordoPage, {});
        break; 
      case 'lototurf':
        this.navCtrl.push(LototurfPage, {});
        break; 
      case 'elige8':
        this.navCtrl.push(Elige8Page, {});
        break; 
       
    }
  }

  guardar () {
    localStorage.setItem('idReceptor', this.idReceptor);
    localStorage.setItem('modoDisparo', this.modoDisparo);
    localStorage.setItem('modoError', this.modoError);
  }
  
}

