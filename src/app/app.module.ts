import { GordoPage } from './../pages/gordo/gordo';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QRScanner } from '@ionic-native/qr-scanner';
import { AuthserviceProvider } from '../providers/authservice/authservice';
import { LoginPage } from '../pages/login/login';
import { BonolotoPage } from '../pages/bonoloto/bonoloto';

import { HttpModule } from '@angular/http';
import { Device } from '@ionic-native/device';
import { EuromillonesProvider } from '../providers/euromillones/euromillones';
import { Vibration } from '@ionic-native/vibration';
import { UnixToLocaleDateFullDays } from '../providers/pipes/unix-to-locale-date-full-days.pipe';
import { DiaSemanaPipe } from '../providers/pipes/dia-semana';
import { DetalleValidacion } from '../pages/detalleValidacion/detalleValidacion';
import { CarriageReturnPipe } from '../providers/pipes/carriageReturn.pipe';
import { IpProvider } from '../providers/ipservice/ipservice';
import { BonolotoProvider } from '../providers/bonoloto/bonoloto';
import { GordoProvider } from '../providers/gordo/gordo';

import { Carrusel } from '../pages/carrusel/carrusel';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { QuinigolPage } from '../pages/quinigol/quinigol';
import { QuinigolProvider } from '../providers/quinigol/quinigol';
import { Insomnia } from '@ionic-native/insomnia';
import { LototurfProvider } from '../providers/lototurf/lototurf';
import { LototurfPage } from '../pages/lototurf/lototurf';
import { FormsModule } from '@angular/forms';
import { LoteriaProvider } from '../providers/loteria/loteria';
import { Elige8Page } from '../pages/elige8/elige8';
import { Elige8Provider } from '../providers/elige8/elige8';

@NgModule({
  declarations: [
    MyApp,
    ConfiguracionPage,
    HomePage,
    BonolotoPage,
    LoginPage,
    Elige8Page,
    GordoPage,
    DetalleValidacion,
    QuinigolPage,
    LototurfPage,
    Carrusel,
    UnixToLocaleDateFullDays,
    DiaSemanaPipe,
    CarriageReturnPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BonolotoPage,
    QuinigolPage,
    LototurfPage,
    ConfiguracionPage,
    DetalleValidacion,
    QuinigolPage,
    Carrusel,
    LoginPage,
    GordoPage,
    Elige8Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QRScanner,
    AuthserviceProvider,
    Device,
    EuromillonesProvider,
    BonolotoProvider,
    LoteriaProvider,
    LototurfProvider,
    QuinigolProvider,
    GordoPage,
    GordoProvider,
    IpProvider,
    Vibration,
    Insomnia,
    Elige8Provider
  ]
})
export class AppModule {}
