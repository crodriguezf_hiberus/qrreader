import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AuthserviceProvider } from '../providers/authservice/authservice';
import { LoginPage } from '../pages/login/login';
import { BonolotoPage } from '../pages/bonoloto/bonoloto';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { QuinigolPage } from '../pages/quinigol/quinigol';
import { Insomnia } from '@ionic-native/insomnia';
import { GordoPage } from '../pages/gordo/gordo';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = ConfiguracionPage;

  pages: Array<{title: string, component: any}>;

  constructor(
    private insomnia: Insomnia,
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    public authService: AuthserviceProvider
  ) {
      this.initializeApp();

      // used for an example of ngFor and navigation
      this.pages = [
        { title: 'Configuracion', component: ConfiguracionPage },
        { title: 'Euromillones', component: HomePage },
        { title: 'Bonoloto', component: BonolotoPage },
        { title: 'Quinigol', component: QuinigolPage },
        { title: 'El Gordo', component: GordoPage }
      ];

      this.insomnia.keepAwake()
        .then(
          () => console.log('keepAwake activado'),
          () => console.log('error activando keepAwake')
        );

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if( window['plugins'] && window['plugins'].NativeAudio ) {
        window['plugins'].NativeAudio.preloadSimple( 'click', 'audio/beep.mp3', function(msg){
        }, function(msg){
          console.log( 'error: ' + msg );
        });
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(){
    this.authService.logout();
    this.nav.setRoot(LoginPage);
  }
}
