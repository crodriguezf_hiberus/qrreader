import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Md5} from 'ts-md5/dist/md5';
import { Device } from '@ionic-native/device';
import { IpProvider } from '../ipservice/ipservice';

/*
  Generated class for the AuthserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthserviceProvider {

  private isLoggedIn;
  public user = {};
  public baseUrl = '192.168.1.136:8080/api';

  constructor(
    public http: Http,
    private ipservice: IpProvider,
    private device: Device) {}

  login(username: string, password: string) {
    password = Md5.hashStr(password).toString();
    let a = {
      "userName": username,
      "deviceName": this.device.model || '',
      "deviceId": this.device.uuid || '',
      "token": "",
      "password": password,
      "client": 1,
      "os": this.device.platform || ''
    };
    this.baseUrl = this.ipservice.getBaseUrl();
    let url = this.baseUrl + '/login';
    return this.http.post(url, a)
      .map(respuesta => {
        if (respuesta && respuesta.status == 200 && respuesta['_body']) {
          let usuario = JSON.parse(respuesta['_body']);

          localStorage.setItem('usuarioQrReader', JSON.stringify(usuario.aliasUsuario));
          localStorage.setItem('tokenQrReader', JSON.stringify(usuario.token));
          this.isLoggedIn = true;
          this.user['name'] = usuario['aliasUsuario'];
        }
        return respuesta;
        });
  }
 
  // Logout a user, destroy token and remove
  // every information related to a user
  logout() : void {
    this.isLoggedIn = null;
    this.user = {};
    localStorage.removeItem('usuarioQrReader');
    localStorage.removeItem('tokenQrReader');
  }
 
  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  isAuthenticated() : boolean {
    if(this.isLoggedIn){
      return this.isLoggedIn; 
    }else{
      let usuario = localStorage.getItem('usuarioQrReader');
      if(usuario){
        this.isLoggedIn = true;
        this.user['name'] = JSON.parse(usuario);
        return true;
      }else{
        return false;
      }
    }
    
  }

}
