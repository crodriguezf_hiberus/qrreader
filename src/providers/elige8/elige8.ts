import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { IpProvider } from '../ipservice/ipservice';
import { of } from "rxjs/observable/of";

/*
  Generated class for the EuromillonesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Elige8Provider {
  
  public baseUrl = 'https://api.eduardolosilla.es';
  
  constructor(public http: Http, private ipservice: IpProvider) {
  }

  enviarNumeroA(obj){
    obj.validacion = parseInt(obj.validacion);
    this.baseUrl = this.ipservice.getBaseUrl();
    let token = JSON.parse(localStorage.getItem('tokenQrReader'));
    let myHeaders = new Headers;
    myHeaders.set('Authorization', token);
    return this.http.post(this.baseUrl + '/control/elige8/asignarNumeroA', obj, {headers: myHeaders});
  }

  obtenerInfoSorteo(){
    this.baseUrl = this.ipservice.getBaseUrl();
    let token = JSON.parse(localStorage.getItem('tokenQrReader'));
    let myHeaders = new Headers;
    myHeaders.set('Authorization', token);
    if (token) {
      return this.http.get(this.baseUrl + '/control/elige8/infoSorteo', {headers: myHeaders});
    } else {
      return of(null);
    }
  }

}
