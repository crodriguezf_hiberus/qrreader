import { Injectable } from '@angular/core';

/*
  Generated class for the AuthserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IpProvider {

  public baseUrl = 'https://192.168.1.136:8080/api';// 'http://192.168.1.136:8080/api';

  constructor() {
    let a = localStorage.getItem('urlServidor');
    if (a) {
      this.baseUrl = a;
    }

  }

  public getBaseUrl () {
    return this.baseUrl;
  }

  public setBaseUrl (url) {
    this.baseUrl = url;
    localStorage.setItem('urlServidor', this.baseUrl);
  }

}
