import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { IpProvider } from '../ipservice/ipservice';
import { of } from "rxjs/observable/of";

/*
  Generated class for the EuromillonesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GordoProvider {
  
  public baseUrl = 'https://api.eduardolosilla.es';// 'http://192.168.1.136:8080/api';
  
  constructor(public http: Http, private ipservice: IpProvider) {
  }


  obtenerInfoSorteo(){
    this.baseUrl = this.ipservice.getBaseUrl();
    let token = JSON.parse(localStorage.getItem('tokenQrReader'));
    let myHeaders = new Headers;
    myHeaders.set('Authorization', token);
    if (token) {
      return this.http.get(this.baseUrl + '/infoSorteoGordo', {headers: myHeaders});
    } else {
      return of(null);
    }
  }
  enviarNumeroA(obj){
    obj.validacion = parseInt(obj.validacion);
    this.baseUrl = this.ipservice.getBaseUrl();
    let token = JSON.parse(localStorage.getItem('tokenQrReader'));
    let myHeaders = new Headers;
    myHeaders.set('Authorization', token);
    return this.http.post(this.baseUrl + '/control/gordo/asignarNumeroA', obj, {headers: myHeaders});
  }


  

}
