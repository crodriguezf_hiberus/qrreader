import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'diaSemana'
})
export class DiaSemanaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
        case 0:
            return 'DOMINGO';
        
        case 1:
            return 'LUNES';
        
        case 2:
            return 'MARTES';
        
        case 3:
            return 'MIÉRCOLES';
        
        case 4:
            return 'JUEVES';
        
        case 5:
            return 'VIERNES';
        
        case 6:
            return 'SÁBADO';
        
    }
  }

}