import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'carriageReturn'
})
export class CarriageReturnPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = value.replace(/[\n]/g, '<br />');
    return value;
  }

}