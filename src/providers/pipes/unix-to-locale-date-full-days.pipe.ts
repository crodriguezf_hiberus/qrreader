import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unixToLocaleDateFullDays'
})
export class UnixToLocaleDateFullDays implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === '') {
      return '---';
    } else {
      const d = new Date(value * 1000);
      let dia: any = d.getDate();
      if (dia.toString().length === 1) { dia = '0' + dia; }
      let mes: any = d.getMonth() + 1;
      if (mes.toString().length === 1) {mes = '0' + mes; }
      const ano = d.getFullYear();
      return dia + '/' + mes  + '/' + ano;
    }
  }

}